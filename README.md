# Conding Dojo - KIBANA

Relaizado 07/08/2020 no auditório da Susam
com o objetivo de compartilhar o conhecimento adquirido 
da ferramenta KIBANA para criação de dashboards.

O objetivo foi criar visualizações que ao final
serviriam para compor um dashboad. 
Passos do Coding Dojo:
1. Quantos eletrônicos foram informados?
2. Quantas Unidades de Saúde responderam o formulário?
3. Quantos eletrônicos foram informados por Unidade de saúde?
4. Qual a Porcentagem de eletrônicos por Unidade de Saúde?
5. Montando uma tabela detalhada
6. Criando um título descritivo
7. Montando Dashboard

Para ter acesso ao dashboad criado acesse:
[Nosso kibana](https://salasituacao.saude.am.gov.br:5602/)



Caso ainda não tenha usuário e senha, mande email para:

Thaian Azambuja da Costa <thaian.azambuja@saude.am.gov.br>;

[Download da Apresentação](https://gitlab.com/AzambujaTC/conding-dojo-kibana/-/raw/master/mincurso_kibana.pdf?inline=false)

Fotos:
![Platéia](foto1.jpeg)
![Platéia](claudino.jpeg)
![Platéia](metric.jpeg)
![Platéia](thaian.jpeg)
![Platéia](plateia.jpeg)
![Platéia](dashboard.jpeg)
![Platéia](WhatsApp_Image_2020-08-12_at_11.53.15.jpeg)